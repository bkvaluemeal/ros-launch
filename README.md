ROS Launch
----------

A collection of launch files I made for ROS

Install
-------

	apt-get install python-catkin-tools
	git clone https://bitbucket.org/bkvaluemeal/ros-launch.git
	catkin build
